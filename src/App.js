import React from "react";
import "./App.css";

import { useDispatch, useSelector } from "react-redux";
import {
  Increment_Req,
  IncrementAsync_Req,
  Decrement_Req,
  Add_Req,
} from "./redux/actionTypes";

const App = () => {
  const dispatch = useDispatch();
  const action = (type, payload) => dispatch({ type, payload });
  const counterReducer = useSelector(({ counterReducer }) => counterReducer);

  return (
    <>
      <button onClick={() => action(IncrementAsync_Req)}>
        Increment after 1 second
      </button>
      <button onClick={() => action(Increment_Req)}>Increment</button>
      <button onClick={() => action(Decrement_Req)}>Decrement</button>
      <button onClick={() => action(Add_Req, 10)}>Add</button>
      <hr />
      <div>Clicked: {counterReducer.count}</div>
    </>
  );
};

export default App;
