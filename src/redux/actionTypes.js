export const Add = "Add";
export const Add_Req = "Add_Req";
export const Increment = "Increment";
export const Increment_Req = "Increment_Req";
export const IncrementAsync_Req = "IncrementAsync_Req";
export const Decrement = "Decrement";
export const Decrement_Req = "Decrement_Req";
