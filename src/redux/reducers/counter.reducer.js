import { Increment, Decrement, Add } from "../actionTypes";

const initialState = {
  count: 0,
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case Increment:
      return { ...state, count: state.count + 2 };
    case Decrement:
      return { ...state, count: state.count - 1 };
    case Add:
      return { ...state, count: state.count + payload };

    default:
      return state;
  }
};
