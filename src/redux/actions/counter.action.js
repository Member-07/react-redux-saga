import { Add, Increment, Decrement } from "../actionTypes";

const setIncrementAction = () => ({
  type: Increment,
});
const setDecrementAction = () => ({
  type: Decrement,
});
const setAddAction = (value) => ({
  type: Add,
  payload: value,
});

export const onIncrement = () => {
  return (dispatch) => {
    dispatch(setIncrementAction());
  };
};

export const onDecrement = () => {
  return (dispatch) => {
    dispatch(setDecrementAction());
  };
};

export const onAdd = (value) => {
  return (dispatch) => {
    dispatch(setAddAction(value));
  };
};

const delay = (ms) => new Promise((res) => setTimeout(res, ms));

export const onIncrementAsync = () => {
  return async (dispatch) => {
    await delay(1000);
    dispatch(setIncrementAction());
  };
};
