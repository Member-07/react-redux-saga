import { put, delay } from "redux-saga/effects";
import { Increment, Decrement, Add } from "../actionTypes";

export function* setIncrementAction() {
  yield put({ type: Increment });
}
export function* setDecrementAction() {
  yield put({ type: Decrement });
}
export function* setAddAction({ payload }) {
  yield put({ type: Add, payload });
}
export function* setIncrementAsynsAction() {
  yield delay(3000);
  yield put({ type: Increment });
}
