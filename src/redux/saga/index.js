import { takeEvery, all } from "redux-saga/effects";
import {
  Increment_Req,
  IncrementAsync_Req,
  Decrement_Req,
  Add_Req,
} from "../actionTypes";
import {
  setAddAction,
  setIncrementAction,
  setDecrementAction,
  setIncrementAsynsAction,
} from "./counter.action";

function* watchIncrementAction() {
  yield takeEvery(Increment_Req, setIncrementAction);
}

function* watchIncrementAsyncAction() {
  yield takeEvery(IncrementAsync_Req, setIncrementAsynsAction);
}

function* watchDecrementAction() {
  yield takeEvery(Decrement_Req, setDecrementAction);
}

function* watchAddAction() {
  yield takeEvery(Add_Req, setAddAction);
}

export default function* rootSaga() {
  yield all([
    watchIncrementAction(),
    watchDecrementAction(),
    watchIncrementAsyncAction(),
    watchAddAction(),
  ]);
}
